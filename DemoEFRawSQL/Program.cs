﻿using Microsoft.EntityFrameworkCore;

AppDbContext context = new AppDbContext();

List<User> users = context.Users.FromSqlRaw("SELECT * FROM USERS").ToList();

foreach (User item in users)
{
    Console.WriteLine($"{item.Id} {item.UserName}");
}

// demo sql injection
string username = "A";
string password = "a";
string sqlRaw = $"SELECT * FROM USERS WHERE Username = '{username}' AND Password = '{password}'";
User user = context.Users.FromSqlRaw(sqlRaw).FirstOrDefault();

if(user is not null)
{
    Console.WriteLine($"{user.UserName} success login");
} else
{
    Console.WriteLine("failed login");
}